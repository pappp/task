package ums.code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Task2 {
	private static HashMap<Integer, String> expressionMap = new HashMap<Integer, String>();
	private static HashMap<Integer, String> operandsMap = new HashMap<Integer, String>();
	private static ScriptEngineManager mgr = new ScriptEngineManager();
	private static ScriptEngine engine = mgr.getEngineByName("JavaScript");
	private static String outputValue ;
	private static int evaluatedCount;
	private static Integer expectedOutput;

	public static void main(String[] args) {

		/***
		 * Prompt for numeric inputs
		 */

		Scanner reader = new Scanner(System.in);
		System.out.println("Enter 6 numbers separated by white space : ");
		String ipNumbers = reader.nextLine();
		System.out.println("Enter desired output value : ");
		outputValue = reader.nextLine();

		reader.close();

		System.out.println("IP --->");
		System.out.println(ipNumbers);
		System.out.println("OP --->");
		System.out.println(outputValue);
		expectedOutput = Integer.parseInt(outputValue);
		/***
		 * Validate the numeric inputs
		 */
		try {
			String[] numbers = ipNumbers.toString().split(" ");
			for (String num : numbers) {
				Integer.parseInt(num);
			}

//			if (numbers.length != 6) {
//				System.out.println("ERROR : Require Min. 6 numbers space delimited.");
//				return;
//			}
			Integer.parseInt(outputValue);

		} catch (Exception e) {
			System.out.println("ERROR : Require Min. 6 numbers space delimited and a valid desired output.");
			return;
		}

		/***
		 * build expressions to be evaluated
		 */
		// Load the numbers to a stack
		List<Integer> numberStack = new ArrayList<>();
		for (String num : ipNumbers.toString().split(" ")) {
			numberStack.add(numberStack.size(), Integer.parseInt(num));
		}

		possibleStrings(5, Arrays.asList("+", "-", "*", "/"), "");
		System.out.println("Size of operands map : " + operandsMap.size());
		System.out.println("evaluatedCount : " + evaluatedCount + " run time : " + new Date());

		permute(numberStack, 0);
		System.out.println("Size of expression map : " + expressionMap.size());
		
	}


	private static void permute(List<Integer> arr, int k) {

		for (int i = k; i < arr.size(); i++) {
			Collections.swap(arr, i, k);
			permute(arr, k + 1);
			Collections.swap(arr, k, i);
		}
		if (k == arr.size() - 1) {
			//System.out.println(java.util.Arrays.toString(arr.toArray()));

			for ( String operands : operandsMap.values()) {
				String expression = new String();
				int o = 0;
				for (Integer integer : arr) {
					expression += "" + integer ;
					if(o < operands.length())
						expression += operands.charAt(o++) ;
				}
				
				try {
//				   System.out.println(engine.eval(expression));
					evaluatedCount++;
				   Integer evaluatedResult =  Integer.parseInt("" +  engine.eval(expression));
				   
				   if(expectedOutput.equals(evaluatedResult)) {
						expressionMap.put(expressionMap.size(), Arrays.toString(arr.toArray()));
				   }
				} catch (Exception e) {
					//System.out.println("Error in evaluating expression : " + expression);
				}
				
//				System.out.println(expression);
				if(evaluatedCount % 1000 == 0) {
					System.out.println("evaluatedCount : " + evaluatedCount + " run time : " + new Date());
				}
			}	
		}
		
	}
	
	
	 public static void possibleStrings(int maxLength, List<String> alphabets, String curr) {
        // If the current string has reached it's maximum length
        if(curr.length() == maxLength) {
//            System.out.println(curr);
            operandsMap.put(operandsMap.size(), curr);
        // Else add each letter from the alphabet to new strings and process these new strings again
        } else {
            for(int i = 0; i < alphabets.size(); i++) {
                String oldCurr = curr;
                curr += alphabets.get(i);
                possibleStrings(maxLength,alphabets,curr);
                curr = oldCurr;
            }
        }
    }
}
