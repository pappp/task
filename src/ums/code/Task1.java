package ums.code;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 {

	/**
	 * @param args
	 */
	static int snakedMapOfNumbersSize = 0;
	static Integer snakeRoot ;
	static Integer[][] snakedMapOfNumbers;
	
	public static void main(String[] args) {

		/***
		 * Prompt for user input.
		 */
		snakeRoot = getUserInput();
		Integer[][] mapOfNumbers = new Integer[snakeRoot][snakeRoot];
		
		int z = 0;
		for (int i = 0; i < snakeRoot; i++) {
			for (int j = 0; j < snakeRoot; j++) {
				mapOfNumbers[i][j] = ++z;
			}
		}

		System.out.println("IP --->");
		for (int i = 0; i < mapOfNumbers.length; i++) {
			Integer[] integers = mapOfNumbers[i];
			System.out.println(Arrays.toString(integers));
		}

		int i, iFirstRow = 0, iFirstColumn = 0, iFinalRow = snakeRoot, iFinalColumn = snakeRoot;
		
		/*
		 * k - starting row index m - ending row index l - starting column index n -
		 * ending column index i - iterator
		 */
		
		snakedMapOfNumbers = new Integer[snakeRoot][snakeRoot];
		while (iFirstRow < iFinalRow && iFirstColumn < iFinalColumn) {
			
			// First row
			for (i = iFirstColumn; i < iFinalColumn; ++i) {
//				System.out.print(a[k][i] + " ");
				snakedMapOfNumbers(mapOfNumbers[iFirstRow][i], iFirstRow, i); 
				snakedMapOfNumbers[iFirstRow][i] = snakedMapOfNumbersSize; 
			}
			iFirstRow++;

			// Last column
			for (i = iFirstRow; i < iFinalRow; ++i) {
//				System.out.print(a[i][n - 1] + " ");
				snakedMapOfNumbers(mapOfNumbers[i][iFinalColumn - 1], i, iFinalColumn-1 ); 
				snakedMapOfNumbers[i][iFinalColumn-1]   = snakedMapOfNumbersSize ; 

			}
			iFinalColumn--;

			// Last row
			if (iFirstRow < iFinalRow) {
				for (i = iFinalColumn - 1; i >= iFirstColumn; --i) {
//					System.out.print(a[m - 1][i] + " ");
					snakedMapOfNumbers(mapOfNumbers[iFinalRow - 1][i], iFinalRow-1, i );
					snakedMapOfNumbers[iFinalRow-1][i] = snakedMapOfNumbersSize;
				}
				iFinalRow--;
			}

			// First column
			if (iFirstColumn < iFinalColumn) {
				for (i = iFinalRow - 1; i >= iFirstRow; --i) {
//					System.out.print(a[i][l] + " ");
					snakedMapOfNumbers(mapOfNumbers[i][iFirstColumn] , i , iFirstColumn); 
					snakedMapOfNumbers[i][iFirstColumn] = snakedMapOfNumbersSize ; 
				}
				iFirstColumn++;
			}
		}
		
		System.out.println("OP --->");
		for (int ii = 0; ii < snakedMapOfNumbers.length; ii++) {
			Integer[] integers = snakedMapOfNumbers[ii];
			System.out.println(Arrays.toString(integers));
		}

		
	
	}

	private static Integer getUserInput() {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int n = reader.nextInt();
		reader.close();
		return Integer.valueOf(n);
	}

	static void snakeWisePrint(Integer snakeRoot, Integer a[][]) {}

	private static void snakedMapOfNumbers(Integer entry, int p1, int p2) {
		snakedMapOfNumbersSize++;
	}
}
