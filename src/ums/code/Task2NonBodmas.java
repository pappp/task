package ums.code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Task2NonBodmas {
	private static HashMap<Integer, String> expressionMap = new HashMap<Integer, String>();
	private static HashMap<Integer, String> operandsMap = new HashMap<Integer, String>();
	private static String outputValue ;
	private static int evaluatedCount;
	private static Integer expectedOutput;

	public static void main(String[] args) {

		/***
		 * Prompt for numeric inputs
		 */

		Scanner reader = new Scanner(System.in);
		System.out.println("Enter 6 numbers separated by white space : ");
		String ipNumbers = reader.nextLine();
		System.out.println("Enter desired output value : ");
		outputValue = reader.nextLine();

		reader.close();

		System.out.println("IP --->");
		System.out.println(ipNumbers);
		System.out.println("OP --->");
		System.out.println(outputValue);
		expectedOutput = Integer.parseInt(outputValue);
		/***
		 * Validate the numeric inputs
		 */
		try {
			String[] numbers = ipNumbers.toString().split(" ");
			for (String num : numbers) {
				Integer.parseInt(num);
			}

			if (numbers.length != 6) {
				System.out.println("ERROR : Require Min. 6 numbers space delimited.");
				return;
			}
			Integer.parseInt(outputValue);

		} catch (Exception e) {
			System.out.println("ERROR : Require Min. 6 numbers space delimited and a valid desired output.");
			return;
		}

		/***
		 * build expressions to be evaluated
		 */
		// Load the numbers to a stack
		List<Integer> numberStack = new ArrayList<>();
		for (String num : ipNumbers.toString().split(" ")) {
			numberStack.add(numberStack.size(), Integer.parseInt(num));
		}

		//Build the permutations of the expressions
		possibleStrings(5, Arrays.asList("+", "-", "*", "/"), "");
		System.out.println("Size of operands map : " + operandsMap.size());
		
		System.out.println("evaluatedCount : " + evaluatedCount + " run time : " + new Date());
		
		//Build the permutations of the input numbers
		permute(numberStack, 0);
		System.out.println("Size of expression map : " + expressionMap.size());
		System.out.println("evaluatedCount : " + evaluatedCount + " run time : " + new Date());

		//Output the evaluated expressions.
		for ( String opExp : expressionMap.values()) {
			System.out.println(opExp  + " = " + expectedOutput);
		}
		
	}


	/***
	 * Accepts user provided input of 6 number & generates all the permutations of the number (6!)
	 * @param arr  : User provided numbers
	 * @param k : min number for generating the permutations
	 */
	private static void permute(List<Integer> arr, int k) {

		for (int i = k; i < arr.size(); i++) {
			Collections.swap(arr, i, k);
			permute(arr, k + 1);
			Collections.swap(arr, k, i);
		}
		if (k == arr.size() - 1) {
			//System.out.println(java.util.Arrays.toString(arr.toArray()));

			for ( String operands : operandsMap.values()) {
				
				String expressionTrak = new String();
				int oo = 0;
				for (Integer integer : arr) {
					expressionTrak += "" + integer ;
					if(oo < operands.length())
						expressionTrak += operands.charAt(oo++) ;
				}
				
				Integer result = getEvaluatedResult(arr, operands);
				
				if(result.equals(expectedOutput)) {
					expressionMap.put(expressionMap.size(), expressionTrak);
				}
				
				evaluatedCount++;
				if(evaluatedCount % 100000 == 0) {
					System.out.println("evaluatedCount : " + evaluatedCount + " run time : " + new Date());
				}
			}	
		}
		
	}

	/***
	 * Method to evaluate the net result of the expression (needs refactor, working now for the spec. input)
	 * @param arr : Array of numbers
	 * @param operands : Array of operands
	 * @return
	 */
	private static Integer getEvaluatedResult(List<Integer> arr, String operands) {
		Integer o1 = arr.get(0);
		Integer o2 = arr.get(1);
		char oper = operands.charAt(0);
		
		Integer result = evaluate(o1, o2, oper);
		
		o2 =  arr.get(2);
		oper = operands.charAt(1);
		result = evaluate(result, o2, oper);
		
		o2 =  arr.get(3);
		oper = operands.charAt(2);
		result = evaluate(result, o2, oper);
		
		o2 =  arr.get(4);
		oper = operands.charAt(3);
		result = evaluate(result, o2, oper);
		
		o2 =  arr.get(5);
		oper = operands.charAt(4);
		result = evaluate(result, o2, oper);
		return result;
	}
	
	/****
	 * Evaluates the output of the expression for the parameters
	 * @param o1 : first numeric value for the operation
	 * @param o2 : second numeric value for the operation
	 * @param oper : Operand
	 * @return return value of o1 oper o2
	 */
	private static int evaluate(Integer o1, Integer o2, char oper) {

		int result = 0;

		switch (oper) {
		case '+':
			result = o1 + o2;
			break;
		case '-':
			result = o1 - o2;
			break;
		case '/':
			result = o1 / o2;
			break;
		case '*':
			result = o1 * o2;
			break;

		default:
			break;
		}

		return result;
	}

	/***
	 * Builds a map of all possible expressions using +, *, -, / with a min length of 5.
	 * @param maxLength : 5
	 * @param alphabets : + * - /
	 * @param curr : empty string
	 */
	public static void possibleStrings(int maxLength, List<String> alphabets, String curr) {
        // If the current string has reached it's maximum length
        if(curr.length() == maxLength) {
//            System.out.println(curr);
            operandsMap.put(operandsMap.size(), curr);
        // Else add each letter from the alphabet to new strings and process these new strings again
        } else {
            for(int i = 0; i < alphabets.size(); i++) {
                String oldCurr = curr;
                curr += alphabets.get(i);
                possibleStrings(maxLength,alphabets,curr);
                curr = oldCurr;
            }
        }
    }
}
