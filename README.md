# ums

The project has 3 classes . 

UMS Developer Exercise
Below is a set of 2 problems that you are expected to solve by writing Java program. To complete this exercise you must solve both problems.
- The code must be well written and must be self-explanatory. Please provide comments to help the evaluator understand your thinking process.
- You may submit an incomplete code, but you must mention that your code is not complete when you make a submission.
- You will be evaluated on the following: a. Ability to write code for all potential cases. b. thinking process. c. Ability to write beautiful code.
- Please note that subsequent rounds of evaluation will deep dive into your solution, so please spend good amount of time in understanding the problem and solution.
Problem 1: Snake Problem
Your objective is to program a snake made of numeral points that keeps growing. Your program must accept a number as input. The number of points on the snake must be the square of the number. This snake starts with the number 1 on the top left and grows in a clockwise direction until it reaches the last number (square of the input). The result looks like a matrix as shown in examples below. The matrix must not have any gaps and must be a perfect square.
For eg.
Sample Input: 3
Sample Output:
1 2 3
8 9 4
7 6 5
Sample Input: 4
Sample Output:
1 2 3 4
12 13 14 5
11 16 15 6
10 9 8 7
Sample Input: 5
Sample Output:
1 2 3 4 5
16 17 18 19 6
15 24 25 20 7
14 23 22 21 8
13 12 11 10 9
You can write the program here: https://www.tutorialspoint.com/compile_java_online.php and when finished click on share code button. A link will be generated share the link back to the person who sent you this problem.
Problem 2: Compute the target
Write a program which accepts a set of 6 positive integers (say x1, x2, x3, x4, x5, x6) separated by whitespace as input. The input ends when the user presses the enter key after entering the 6th integer. On a new line the program must accept another positive integer as input (say y).
Now, the program must use 4 operators (+, -, / , *) on the 6 input numers (x1 to x6) in any order, so that the result of the operation is y. All the six numbers must be used in any order. However, it is not necessary to use all operators.
If a set of input numbers has multiple solutions, you must print all possible solutions in its own line (see sample output below). If a set of numbers is not solvable, you must print a message indicating the same.
The program will NOT follow BODMAS rules, instead the calculation will be done from left to right in linear fashion. For eg. 4 + 4 / 2 * 4 � 1*3 = 8 / 2 * 4 � 1*3 = 4 * 4 � 1*3 = 16 � 1*3 = 45.
For eg.
Sample Input
1 3 7 6 8 3
250
Sample Output
3 + 3 * 7 + 1 * 6 - 8 = 250
3 + 8 * 7 + 6 * 3 + 1 = 250
7 / 3 + 3 * 8 - 1 * 6 = 250
8 + 3 * 7 + 6 * 3 + 1 = 250
You can write the program here: https://www.tutorialspoint.com/compile_java_online.php and when finished click on share code button. A link will be generated share the link back to the person who sent you this problem.